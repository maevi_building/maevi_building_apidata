// ENVIRONMENT
const {
    region,
    infoResourcesTableName,
    dataLoggerTableName,
    ttlDataDownloadTableName,
    saveDownloadBucket,
} = require("./config");

// LIBRARY
const chromium = require('chrome-aws-lambda');
const handleBars = require('handlebars')
const AWS = require("aws-sdk");
const lodash = require('lodash');
const moment = require('moment');
const q = require("q");
const shortid = require('shortid');
const fs = require('fs')
const path = require('path')
const utils = require('util')
const readFile = utils.promisify(fs.readFile)

// AWS SERVICES
const docClient = new AWS.DynamoDB.DocumentClient({
	apiVersion: "2012-08-10",
	region: process.env.REGION
});
const s3 = new AWS.S3({
	apiVersion: '2006-03-01',
	region: process.env.REGION
});

// GLOBAL FUNCTION
function throwError (message) {
    var payload = {
        statusCode: 400, 
        message: message,
    }
    return payload;
}
async function errorPayload (statusCode, message) {
    try {
        var payload = {
            statusCode: statusCode,
            headers: {
                "Content-Type": "application/json",
                "x-amzn-ErrorType": statusCode
            },
            isBase64Encoded: false,
            body: JSON.stringify({
                message: message,
                statusCode: statusCode,
            })
        }

        return payload
    }
    catch (error) {
        throw error
    }
}
async function successPayload (type, input) {
    try {
        var body = {};
        switch (type) {
            case 'data':
                body = {
                    data: input,
                    statusCode: 200,
                }
                break;
            case 'message':
                body = {
                    message: input,
                    statusCode: 200,
                }
                break;
        }

        var payload = {
            statusCode: 200,
            headers: {
                "Content-Type": "application/json"
            },
            isBase64Encoded: false,
            body: JSON.stringify(body),
        }

        return payload
    }
    catch (error) {
        throw error
    }
}

// MAIN FUNCTION
exports.functionHandler = async (event) => {
    try {
        if (!event.pathParameters || !event.body) {
            throw throwError("Not found.")
        }

        let { body } = event;
        const incomingBody = JSON.parse(body);

        let { profileId } = event.pathParameters;
        let { buildingName, deviceList, dateMonth, timeZone } = incomingBody;

        if (!profileId || !buildingName || !deviceList || !dateMonth || !timeZone || deviceList.length == 0) {
            throw throwError("Parameters not found.")
        }

		dateMonth = parseInt(dateMonth)

        // query all info per site
        var params = {
            TableName: infoResourcesTableName,
            IndexName: 'BuildingName-Index',
            FilterExpression: `contains(ResourcesName, :fkey1)`,
            KeyConditionExpression: 'BuildingName = :hkey',
            ExpressionAttributeValues: {
                ':hkey': buildingName,
                ':fkey1': profileId,
            }
        };

        var queryResp = await docClient.query(params).promise();
        var siteInfoList = queryResp.Items;
        
        var buildingInfo = [];
        var listOfDevice = [];

        siteInfoList.forEach(function (eachInfo) {
            // get site info
            if (eachInfo.ResourcesName.includes("Building")) {
                buildingInfo.push({
                    BuildingName: eachInfo.BuildingName, 
                    City: eachInfo.City,
                    State: eachInfo.State,
                    PostCode: eachInfo.PostCode,
                    Country: eachInfo.Country,
                    GPSLocation: eachInfo.GPSLocation,
                    GrossFloorArea: eachInfo.GrossFloorArea,
                });
            }
        })

        // get list of devices
        deviceList.forEach(function (eachDPM) {
            listOfDevice.push({
                Type: eachDPM.ResourcesType,
                Label: eachDPM.ResourcesLabel,
                ResourcesID: eachDPM.ResourcesID,
            });
        })

        // get offset GMT
        var offSetGMT = timeZone.substring(3, 8);

        // get total consumption (this month and last month)
        var dateFromMonthly = moment.utc().utcOffset(offSetGMT).month(dateMonth).endOf('month').add(1, 'milliseconds').toISOString();
        var dateToMonthly = moment.utc().utcOffset(offSetGMT).month(dateMonth).startOf('month').toISOString();
        var dateToStartYear = moment.utc().utcOffset(offSetGMT).startOf('year').endOf('month').add(1, 'milliseconds').toISOString();
        
        var promisesMonthly = [];
        deviceList.forEach(function (eachDPM) {
            var params = {
                TableName: dataLoggerTableName,
                ProjectionExpression: `ResourcesID,ResourcesCreated,EnergyTrend`,
                KeyConditionExpression: 'ResourcesID = :hkey AND ResourcesCreated BETWEEN :todate AND :fromdate',
                ExpressionAttributeValues: {
                    ':hkey': `MaeviB:${eachDPM.ResourcesID}:1MO`,
                    ':fromdate': dateFromMonthly,
                    ':todate': dateToStartYear
                }
            };

            promisesMonthly.push(docClient.query(params).promise());
        })

        // get daily trend this month
		var dateFromThisMonth = moment.utc().utcOffset(offSetGMT).month(dateMonth).endOf('month').add(1, 'milliseconds').toISOString();
		var dateToThisMonth = moment.utc().utcOffset(offSetGMT).month(dateMonth).startOf('month').add(1,'days').toISOString();
		var promisesDailyThisMonth = [];
		deviceList.forEach(function (eachDPM) {
			var params = {
				TableName: dataLoggerTableName,
				ProjectionExpression: `ResourcesID,ResourcesCreated,EnergyTrend`,
				KeyConditionExpression: 'ResourcesID = :hkey AND ResourcesCreated BETWEEN :todate AND :fromdate',
				ExpressionAttributeValues: {
					':hkey': `MaeviB:${eachDPM.ResourcesID}:1D`,
					':fromdate': dateFromThisMonth,
					':todate': dateToThisMonth,
				}
			};

			promisesDailyThisMonth.push(docClient.query(params).promise());
		})

		// await all data query
		var monthlyConsumption = await q.all(promisesMonthly);
		var dailyConsumptionThisMonth = await q.all(promisesDailyThisMonth);
		
		// calculate total consumption
		var totalEnergyThisMonth = 0;
		var totalEnergyLastMonth = 0;
		var totalEnergyThisYear = 0;
		monthlyConsumption.forEach(function(eachData) {
			eachData.Items.forEach(function(eachMeter) {
				// this month
				if (eachMeter.ResourcesCreated == dateFromMonthly) {
					totalEnergyThisMonth = totalEnergyThisMonth + eachMeter.EnergyTrend;
				}
				// last month
				if (eachMeter.ResourcesCreated == dateToMonthly) {
					totalEnergyLastMonth = totalEnergyLastMonth + eachMeter.EnergyTrend;
				}
				// this year
				totalEnergyThisYear = totalEnergyThisYear + eachMeter.EnergyTrend;
			})
		})

		// set to 2 decimal point
		var dateToStartYearValueOf = moment.utc().utcOffset(offSetGMT).startOf('year').valueOf();
		var dateToEndYearValueOf = moment.utc().utcOffset(offSetGMT).endOf('year').add(1, 'milliseconds').valueOf();

		var yearInMillisecond = dateToEndYearValueOf - dateToStartYearValueOf;
		var currentDateToMillisecond = moment.utc().valueOf() - dateToStartYearValueOf;

		totalEnergyThisMonth = parseFloat(totalEnergyThisMonth.toFixed(2));
		totalEnergyLastMonth = parseFloat(totalEnergyLastMonth.toFixed(2));
		totalEnergyThisYear = parseFloat(((totalEnergyThisYear / currentDateToMillisecond) * yearInMillisecond).toFixed(2)); 

		// calculate carbon emission
		// https://www.mgtc.gov.my/wp-content/uploads/2019/12/2017-CDM-Electricity-Baseline-Final-Report-Publication-Version.pdf - page 48
        if (buildingInfo.State == "Sarawak") {
            var carbonEmissionCoefficient = 0.330;
        } else if (buildingInfo.State == "Sabah") {
            var carbonEmissionCoefficient = 0.525;
        } else {
            var carbonEmissionCoefficient = 0.585;
        }
        
		var totalCarbonEmissionThisMonth = parseFloat((totalEnergyThisMonth/1000) * carbonEmissionCoefficient).toFixed(2);
		var totalCarbonEmissionLastMonth = parseFloat((totalEnergyLastMonth/1000) * carbonEmissionCoefficient).toFixed(2);
		var percentageThisMonth = parseFloat( (((totalEnergyThisMonth - totalEnergyLastMonth) / totalEnergyLastMonth) * 100) ).toFixed(2);
		var percentageLastMonth = 0;
		var rounded = parseInt(percentageThisMonth / 100) + 1;
		var maxPercentage = 100 * rounded;
		var minPercentage = -100 * rounded;
		

		// calculate sum daily consumption
		var sumDailyConsumption = [];

		// calculate daily energy trend this month
		dailyConsumptionThisMonth.forEach(function(eachDevice) {
			var i = 0;
			eachDevice.Items.forEach(function(eachEnergy) {
				sumDailyConsumption[i] = sumDailyConsumption[i] === undefined ? {} : sumDailyConsumption[i];
				sumDailyConsumption[i]['date'] = moment(eachEnergy.ResourcesCreated).valueOf()
				
				if (sumDailyConsumption[i]) {
					sumDailyConsumption[i]['value'] = parseFloat(((sumDailyConsumption[i].DailyTrendThisMonth === undefined ? 0 : sumDailyConsumption[i].DailyTrendThisMonth) + eachEnergy.EnergyTrend).toFixed(2))
				} else {
					sumDailyConsumption[i]['value'] = parseFloat((0 + eachEnergy.EnergyTrend).toFixed(2))
				}
				
				sumDailyConsumption[i]['color'] = (moment(sumDailyConsumption[i]['date']).format('dddd') === 'Sunday' || moment(sumDailyConsumption[i]['date']).format('dddd') === 'Saturday') ? "#8167dc" : "#67b7dc"
				i++;
			})
		})

		// find max med min for daily energy trend
		var maxDaily = lodash.maxBy(sumDailyConsumption, function(o) { return o.value !== undefined ? o.value : 0 });
		var minDaily = lodash.minBy(sumDailyConsumption, function(o) { return o.value !== undefined ? o.value : 0 });
		var avgDaily = lodash.meanBy(sumDailyConsumption, function(o) { return o.value !== undefined ? o.value : 0 });

		var lastMonthName = moment().month(dateMonth - 1).startOf("month").format('MMMM')
		var thisMonthName = moment().month(dateMonth).startOf("month").format('MMMM')
		var beiValue = parseFloat((totalEnergyThisYear / buildingInfo[0].GrossFloorArea).toFixed(2))

		var dataOut = {
			BuildingProfile: buildingInfo,
			BEI: {
				BaseLine: 200,
				Value: beiValue,
				Conslusion: beiValue > 200 ? "BEI is forecasted to be higher than yearly baseline. Please monitor your energy consumption." : "BEI is forecasted to be lower than yearly baseline. You are doing great!",
				barColor: beiValue > 200 ? "#e27694" : "#62d071"
			},
			EquipmentProfile: {
				Device: {
					List: listOfDevice,
					Quantity: listOfDevice.length,
				}
			},
			DateGenerated: moment.utc().utcOffset(offSetGMT).format("DD/MM/YYYY"),
			TotalEnergy: {
				LastMonth: totalEnergyLastMonth,
				ThisMonth: totalEnergyThisMonth,
				PercentageLastMonth: percentageLastMonth,
				PercentageThisMonth: isNaN(percentageThisMonth) ? 0 : isFinite(percentageThisMonth) ? percentageThisMonth : 0,
				MaxPercentage: isNaN(maxPercentage) ? 0 : maxPercentage,
				MinPercentage: isNaN(minPercentage) ? 0 : minPercentage,
				EnergyComparisonConclusion: isFinite(percentageThisMonth) ? (percentageThisMonth >= 0 ? `Energy Consumption for ${thisMonthName} is ${percentageThisMonth} % higher than ${lastMonthName}` : `Energy Consumption for ${thisMonthName} is ${percentageThisMonth} % lower than ${lastMonthName}`) : ""
			},
			TotalCarbonEmission: {
				CarbonEmissionCoefficient: carbonEmissionCoefficient,
				LastMonth: totalCarbonEmissionLastMonth,
				ThisMonth: totalCarbonEmissionThisMonth,
			},
			MonthName: {
				LastMonth: lastMonthName,
				ThisMonth: thisMonthName
			},
			DailyAnalysis: {
				MaxDaily: maxDaily !== undefined ? maxDaily.value : 0,
				MinDaily: minDaily !== undefined ? maxDaily.value : 0,
				AvgDaily: isNaN(parseFloat(avgDaily.toFixed(2))) ? 0 : parseFloat(avgDaily.toFixed(2)),
			},
		}
		
		const htmlPath = path.resolve("./html_templates/monthly_report.html");
		var res = await readFile(htmlPath, 'utf8');

		// Now we have the html code of our template in res object
		// you can check by logging it on console
		handleBars.registerHelper('printArray',function(options) {
			return options.fn(sumDailyConsumption);
		});
		const template = await handleBars.compile(res, { strict: true });
		
		// we have compile our code with handlebars
		const result = template(dataOut);

		// We can use this to add dyamic data to our handlebas template at run time from database or API as per need. 
		// you can read the official doc to learn more https://handlebarsjs.com/
		const html = result;

		// we are using headless mode
		await chromium.font('https://dl.dafont.com/dl/?f=robotop');
		const browser = await chromium.puppeteer.launch({
            args: chromium.args,
            defaultViewport: chromium.defaultViewport,
            executablePath: await chromium.executablePath,
            headless: chromium.headless,
            ignoreHTTPSErrors: true,
        });
		const page = await browser.newPage();

		// // add script
		// const corePath = path.resolve("./html_template/lib/4/core.js");
		// const chartsPath = path.resolve("./html_template/lib/4/charts.js");
		// const animatedPath = path.resolve("./html_template/lib/4/themes/animated.js");
		// await page.addScriptTag({ path: corePath });
		// await page.addScriptTag({ path: chartsPath });
		// await page.addScriptTag({ path: animatedPath });

		// We set the page content as the generated html by handlebars	
		await page.setContent(html,{
			waitUntil: [
				"networkidle0",
				// "load",
				// "domcontentloaded",
				// "networkidle2",
			]
		});

		var pdfOut = await page.pdf({ 
			// path: 'Monthly_Report.pdf', 
			format: 'A4',
		})
		await browser.close();

		var uniqId = shortid.generate().replace('-','0').replace('_','0');
		var keyFile = `Monthly_Report_${uniqId}.pdf`

		var params = {
			Body: pdfOut, 
			Bucket: saveDownloadBucket,
			Key: keyFile,
			ContentType: 'application/octet-stream',
			// ContentDisposition: 'attachment',
		};
		await s3.putObject(params).promise();

		// create Presigned URL for direct URL download
		var params = {
			Bucket: saveDownloadBucket,
			Key: keyFile,
			Expires: 3600 //time to expire in seconds - expired in one hour
		}
		var presignedGETURL = s3.getSignedUrl('getObject', params);

		// Save file information into DB TTL to delete file in 1 hour
	    var params = {
			TableName : ttlDataDownloadTableName,
			Item: {
				FileId: uniqId,
				DeviceId: 'MonthlyReport',
				TTL: (moment.utc().milliseconds(0).add(1,'hour').valueOf()) / 1000,
				FileName: keyFile,
			}
		};
		await docClient.put(params).promise();
		
		var output = {
			presignedURL: presignedGETURL
		}
        
        var response = await successPayload('data', output);
        return response;
    }
    catch (error) {
        var response = await errorPayload(error.statusCode, error.message);
        return response;
    }
}

