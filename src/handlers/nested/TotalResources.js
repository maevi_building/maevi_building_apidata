// ENVIRONMENT
const {
    region,
    infoResourcesTableName,
    dataLoggerTableName
} = require("../config");

// LIBRARY
const AWS = require("aws-sdk");
const q = require("q");

// AWS SERVICES
const docClient = new AWS.DynamoDB.DocumentClient({
    apiVersion: "2012-08-10",
    region: region,
});

// GLOBAL FUNCTION


// MAIN FUNCTION
exports.nestedHandler = async (event) => {
    try {
        var payload = event.payload;

        var { profileId } = payload;

        if (!profileId) {
            throw throwError("Parameter not found.")
        }

        var promises = [];

        // get list of building
        var params = {
            TableName: infoResourcesTableName,
            ProjectionExpression: 'BuildingName',
            KeyConditionExpression: 'ResourcesName = :hkey',
            ExpressionAttributeValues: {
                ':hkey': `MaeviB:${profileId}:Building`,
            }
        };

        promises.push(docClient.query(params).promise());

        // get list of gateway
        var params = {
            TableName: infoResourcesTableName,
            ProjectionExpression: 'ResourcesID',
            KeyConditionExpression: 'ResourcesName = :hkey',
            ExpressionAttributeValues: {
                ':hkey': `MaeviB:${profileId}:Gateway`,
            }
        };

        promises.push(docClient.query(params).promise());

        // get list of DPM
        var params = {
            TableName: infoResourcesTableName,
            ProjectionExpression: 'ResourcesID',
            FilterExpression: 'ResourcesType = :fkey',
            KeyConditionExpression: 'ResourcesName = :hkey',
            ExpressionAttributeValues: {
                ':hkey': `MaeviB:${profileId}:Device`,
                ':fkey': 'Power_Meter',
            }
        };

        promises.push(docClient.query(params).promise());

        // get list of THD
        var params = {
            TableName: infoResourcesTableName,
            ProjectionExpression: 'ResourcesID',
            FilterExpression: 'ResourcesType = :fkey',
            KeyConditionExpression: 'ResourcesName = :hkey',
            ExpressionAttributeValues: {
                ':hkey': `MaeviB:${profileId}:Device`,
                ':fkey': 'THD_Sensor',
            }
        };

        promises.push(docClient.query(params).promise());

        var resp = await q.all(promises);

        var respOut = {
            totalSite: resp[0].Items.length,
            totalGateway: resp[1].Items.length,
            totalDPM: resp[2].Items.length,
            totalTHD: resp[3].Items.length,
        };

        return respOut
    }
    catch (error) {
        throw error
    }
}
