// ENVIRONMENT
const {
    region,
    infoResourcesTableName,
    dataLoggerTableName
} = require("../config");

// LIBRARY
const AWS = require("aws-sdk");
const q = require("q");
const moment = require("moment");
var si = require('si-prefix');

// AWS SERVICES
const docClient = new AWS.DynamoDB.DocumentClient({
    apiVersion: "2012-08-10",
    region: region,
});

// GLOBAL FUNCTION


// MAIN FUNCTION
exports.nestedHandler = async (event) => {
    try {
        var payload = event.payload;

        var { profileId, thisMonth, lastMonth, currentDate } = payload;

        if (!profileId || !thisMonth || !lastMonth || !currentDate) {
            throw throwError("Parameter not found.")
        }

        // get list of building
        var params = {
            TableName: infoResourcesTableName,
            ProjectionExpression: 'BuildingName',
            KeyConditionExpression: 'ResourcesName = :hkey',
            ExpressionAttributeValues: {
                ':hkey': `MaeviB:${profileId}:Building`,
            }
        };

        var resp = await docClient.query(params).promise();
        var respOut = {};

        if (resp.Items.length > 0) {
            // get list of devices per building
            var promises = [];

            resp.Items.forEach(async (eachBuilding) => {
                var params = {
                    TableName: infoResourcesTableName,
                    IndexName: 'BuildingName-Index',
                    ProjectionExpression: `ResourcesID, ResourcesCategory`,
                    FilterExpression: `contains(ResourcesName, :fkey1) AND ResourcesType = :fkey2 AND (ResourcesCategory = :fkey4 OR ResourcesCategory = :fkey3)`,
                    KeyConditionExpression: 'BuildingName = :hkey',
                    ExpressionAttributeValues: {
                        ':hkey': eachBuilding.BuildingName,
                        ':fkey1': `${profileId}:Device`,
                        ':fkey2': `Power_Meter`,
                        ':fkey3': 'tier1',
                        ':fkey4': 'MSB',
                    }
                };
                promises.push(docClient.query(params).promise());
            })
            var queryResp = await q.all(promises);

            // reformat the data
            var deviceList = [];
            queryResp.forEach(async (eachResp) => {
                eachResp.Items.forEach(function(eachDevice) {
                    deviceList.push(eachDevice)
                })
            })

            // get EnergyTrend per devices
            var promises = [];
            deviceList.forEach(async (eachDevice) => {
                var params = {
                    TableName: dataLoggerTableName,
                    ProjectionExpression: `EnergyTrend, ResourcesCreated`,
                    KeyConditionExpression: 'ResourcesID = :hkey AND ResourcesCreated BETWEEN :todate AND :fromdate',
                    ScanIndexForward: false,
                    ExpressionAttributeValues: {
                        ':hkey': `MaeviB:${eachDevice.ResourcesID}:1MO`,
                        ':todate': lastMonth,
                        ':fromdate': thisMonth,
                    }
                };
                promises.push(docClient.query(params).promise());
            })
            var queryResp = await q.all(promises);

            var totalThisMonth = 0;
            var totalLastMonth = 0;
            queryResp.forEach(function(eachData) {
                if (eachData.Items.length > 0) {
                    if (eachData.Items[0].ResourcesCreated === thisMonth) {
                        totalThisMonth  = totalThisMonth + eachData.Items[0].EnergyTrend;
                    }
                    else if (eachData.Items[0].ResourcesCreated === lastMonth) {
                        totalLastMonth = totalLastMonth + eachData.Items[0].EnergyTrend;
                    }

                    if (eachData.Items[1] !== undefined) {
                        totalLastMonth = totalLastMonth + eachData.Items[1].EnergyTrend;
                    }
                }
            })

            // plorate totalThisMonth
            var diffMillis = moment(currentDate).valueOf() - moment(lastMonth).valueOf();
            var diffMillisFull = moment(thisMonth).valueOf() - moment(lastMonth).valueOf();
            var thisMonthProrate = (totalThisMonth / diffMillis) * diffMillisFull;

            var percentage = ((thisMonthProrate - totalLastMonth) / totalLastMonth) * 100;

            // scale
            var scale = new si.Scale();
            var unit = new si.Unit(scale, 'Wh');

            // carbon emission
            var coefCarbonEmission = 0.741; // kgCO2/kWh
            var totalCEThisMonth = totalThisMonth * (coefCarbonEmission * 1000);
            var totalCELastMonth = totalLastMonth * (coefCarbonEmission * 1000);
            var thisMonthCEProrate = thisMonthProrate * (coefCarbonEmission * 1000);

            // scale
            var scaleCE = new si.Scale();
            var unitCE = new si.Unit(scale, 'g');

            respOut = {
                totalEnergyThisMonth: parseFloat(totalThisMonth.toFixed(2)),
                unitEnergyThisMonth: `${parseFloat(unit.format(parseFloat((totalThisMonth * 1000).toFixed(2)), ' ').split(' ')[0]).toFixed(2)} ${unit.format(parseFloat((totalThisMonth * 1000).toFixed(2)), ' ').split(' ')[1]}`,
                totalEnergyLastMonth: parseFloat(totalLastMonth.toFixed(2)),
                unitEnergyLastMonth: `${parseFloat(unit.format(parseFloat((totalLastMonth * 1000).toFixed(2)), ' ').split(' ')[0]).toFixed(2)} ${unit.format(parseFloat((totalLastMonth * 1000).toFixed(2)), ' ').split(' ')[1]}`,
                thisMonthEnergyProrate: parseFloat(thisMonthProrate.toFixed(2)),
                unitMonthEnergyProrate: `${parseFloat(unit.format(parseFloat((thisMonthProrate * 1000).toFixed(2)), ' ').split(' ')[0]).toFixed(2)} ${unit.format(parseFloat((thisMonthProrate * 1000).toFixed(2)), ' ').split(' ')[1]}`,
                percentage: parseFloat(percentage.toFixed(2)),
                coefCarbonEmission: coefCarbonEmission,
                yearCarbonEmission: 2015,
                totalCEThisMonth: `${parseFloat(unitCE.format(parseFloat((totalCEThisMonth).toFixed(2)), ' ').split(' ')[0]).toFixed(2)} ${unitCE.format(parseFloat((totalCEThisMonth).toFixed(2)), ' ').split(' ')[1]}`,
                totalCELastMonth: `${parseFloat(unitCE.format(parseFloat((totalCELastMonth).toFixed(2)), ' ').split(' ')[0]).toFixed(2)} ${unitCE.format(parseFloat((totalCELastMonth).toFixed(2)), ' ').split(' ')[1]}`,
                thisMonthCEProrate: `${parseFloat(unitCE.format(parseFloat((thisMonthCEProrate).toFixed(2)), ' ').split(' ')[0]).toFixed(2)} ${unitCE.format(parseFloat((thisMonthCEProrate).toFixed(2)), ' ').split(' ')[1]}`,
            };
        }
    
        return respOut
    }
    catch (error) {
        throw error
    }
}
