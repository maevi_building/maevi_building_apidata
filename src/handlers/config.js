module.exports = {
	// dynamoDB
	infoResourcesTableName: process.env.INFO_RESOURCES_TABLE || 'production-maevibbackend-Resources-1ID6XA4JXDBOK-DynamoDBMaeviBInfoResources-1EOBHEJOCBVCB',
	dataLoggerTableName: process.env.DATA_LOGGER_TABLE || 'production-maevibbackend-Resources-1ID6XA4JXDBOK-DynamoDBMaeviBLogger-1FE6TWQPB6K2M',

	// cloud Info
	region: process.env.REGION || 'ap-southeast-1',
}







