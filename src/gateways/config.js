module.exports = {
	// dynamoDB
	infoResourcesTableName: process.env.INFO_RESOURCES_TABLE_NAME || 'develop-maevibbackend-Resources-1V7L4S5JOWYCV-DynamoDBMaeviBInfoResources-WQFYXU6DMI5U',
	greengrassResourcesTableName: process.env.GREENGRASS_RESOURCES_TABLE_NAME || 'develop-maevibbackend-Resources-1V7L4S5JOWYCV-DynamoDBMaeviBGreenGrassResources-16C8XLK1S659I',

	// cloud Info
	region: process.env.REGION || 'ap-southeast-1',
}