// ENVIRONMENT
const {
    region,
    infoResourcesTableName,
} = require("./config");

// LIBRARY
const AWS = require("aws-sdk");
const lodash = require("lodash");

// AWS SERVICES
const docClient = new AWS.DynamoDB.DocumentClient({
    apiVersion: "2012-08-10",
    region: region,
});

// GLOBAL FUNCTION
function throwError (message) {
    var payload = {
        statusCode: 400, 
        message: message,
    }
    return payload;
}
async function errorPayload (statusCode, message) {
    try {
        var payload = {
            statusCode: statusCode,
            headers: {
                "Content-Type": "application/json",
                "x-amzn-ErrorType": statusCode
            },
            isBase64Encoded: false,
            body: JSON.stringify({
                message: message,
                statusCode: statusCode,
            })
        }

        return payload
    }
    catch (error) {
        throw error
    }
}
async function successPayload (type, input) {
    try {
        var body = {};
        switch (type) {
            case 'data':
                body = {
                    data: input,
                    statusCode: 200,
                }
                break;
            case 'message':
                body = {
                    message: input,
                    statusCode: 200,
                }
                break;
        }

        var payload = {
            statusCode: 200,
            headers: {
                "Content-Type": "application/json"
            },
            isBase64Encoded: false,
            body: JSON.stringify(body),
        }

        return payload
    }
    catch (error) {
        throw error
    }
}

// MAIN FUNCTION
exports.functionHandler = async (event) => {
    try {
    	if (!event.pathParameters || !event.queryStringParameters) {
            throw throwError("Not found.")
    	}

    	let { profileId } = event.pathParameters;
        let { siteName } = event.queryStringParameters;

    	if (!profileId || !siteName) {
    		throw throwError("Parameters not found.")
    	}

        // Check if sites has been registered.
        var params = {
            TableName : infoResourcesTableName,
            ProjectionExpression: `BuildingName`,
            KeyConditionExpression: 'ResourcesName = :hkey',
            FilterExpression: 'BuildingName = :fkey',
            ExpressionAttributeValues: {
                ':hkey': `MaeviB:${profileId}:Building`,
                ':fkey': siteName,
            }
        };

        var queryResp = await docClient.query(params).promise();

        if (queryResp.Items.length == 0) {
            throw throwError("Site not found.")
        } else {
            // Query Power Meter per Building Name
            var params = {
                TableName : infoResourcesTableName,
                IndexName: 'BuildingName-Index',
                ProjectionExpression: `ResourcesCategory`,
                KeyConditionExpression: 'BuildingName = :hkey',
                FilterExpression: 'ResourcesName = :fkey1 AND ResourcesType = :fkey2',
                ExpressionAttributeValues: {
                    ':hkey': siteName,
                    ':fkey1': `MaeviB:${profileId}:Device`,
                    ':fkey2': 'Power_Meter'
                }
            }; 
            var queryResp = await docClient.query(params).promise();

            var unionArray = lodash.unionBy(queryResp.Items, 'ResourcesCategory');
            var response = await successPayload('data', unionArray);
		    return response;
        }
    }
    catch (error) {
        var response = await errorPayload(error.statusCode, error.message);
        return response;
    }
}
