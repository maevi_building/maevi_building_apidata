// ENVIRONMENT
const {
    region,
    infoResourcesTableName,
} = require("./config");

// LIBRARY
const AWS = require("aws-sdk");
const lodash = require("lodash");
const crypto = require("crypto");
const moment = require("moment");

// AWS SERVICES
const docClient = new AWS.DynamoDB.DocumentClient({
    apiVersion: "2012-08-10",
    region: region,
});

// GLOBAL FUNCTION
function throwError (message) {
    var payload = {
        statusCode: 400, 
        message: message,
    }
    return payload;
}
async function errorPayload (statusCode, message) {
    try {
        var payload = {
            statusCode: statusCode,
            headers: {
                "Content-Type": "application/json",
                "x-amzn-ErrorType": statusCode
            },
            isBase64Encoded: false,
            body: JSON.stringify({
                message: message,
                statusCode: statusCode,
            })
        }

        return payload
    }
    catch (error) {
        throw error
    }
}
async function successPayload (type, input) {
    try {
        var body = {};
        switch (type) {
            case 'data':
                body = {
                    data: input,
                    statusCode: 200,
                }
                break;
            case 'message':
                body = {
                    message: input,
                    statusCode: 200,
                }
                break;
        }

        var payload = {
            statusCode: 200,
            headers: {
                "Content-Type": "application/json"
            },
            isBase64Encoded: false,
            body: JSON.stringify(body),
        }

        return payload
    }
    catch (error) {
        throw error
    }
}
function genRandomString() {
    var length = 20;
    return crypto.randomBytes(Math.ceil(length/2))
            .toString('hex') /** convert to hexadecimal format */
            .slice(0,length); /** return required number of characters */
}
function HashSha256(accesskey, salt) {
    var hash = crypto.createHmac('sha256', salt); /** Hashing algorithm sha256 */
    hash.update(accesskey);
    var value = hash.digest('hex');

    return value;
}

// MAIN FUNCTION
exports.functionHandler = async (event) => {
    try {
        if (!event.pathParameters || !event.body) {
            throw throwError("Not found.")
        }

        let { body } = event;
        const incomingBody = JSON.parse(body);

        let { profileId } = event.pathParameters;
        let { ResourcesEntity } = incomingBody;

        if (!profileId || !ResourcesEntity) {
            throw throwError("Parameters not found.")
        }

        // Check if have gateways.
        var params = {
            TableName : infoResourcesTableName,
            ProjectionExpression: `ResourcesID`,
            KeyConditionExpression: 'ResourcesName = :hkey AND ResourcesEntity = :rkey',
            ExpressionAttributeValues: {
                ':hkey': `MaeviB:${profileId}:Gateway`,
                ':rkey': ResourcesEntity
            }
        };

        var queryResp = await docClient.query(params).promise();

        if (queryResp.Items.length == 0) {
            throw throwError("Gateway not found.")
        }
        else {
            // proceed to update
            var updateExpression = [];
            var expressionAttributeNames = {};
            var needUpdate = false;

            expressionAttributeValues = {};
            
            for (key in incomingBody) {
                if (key != "ResourcesEntity") {
                    needUpdate = true;
                    updateExpression.push(`#${key} = :${key}`);
                    expressionAttributeNames[`#${key}`] = key;
                    expressionAttributeValues[`:${key}`] = incomingBody[key];
                }
            }

            // update time
            updateExpression.push(`#UpdateTime = :UpdateTime`);
            expressionAttributeNames[`#UpdateTime`] = "UpdateTime";
            expressionAttributeValues[`:UpdateTime`] = moment.utc().startOf('minutes').toISOString();
            updateExpression = 'set ' + updateExpression.join(",");

            if (needUpdate) {
                var params = {
                    TableName : infoResourcesTableName,
                    Key: {
                        ResourcesName: `MaeviB:${profileId}:Gateway`,
                        ResourcesEntity: ResourcesEntity
                    },
                    UpdateExpression: updateExpression,
                    ExpressionAttributeNames: expressionAttributeNames,
                    ExpressionAttributeValues: expressionAttributeValues
                }
                await docClient.update(params).promise();

                // query all gateways per profileid
                var params = {
                    TableName : infoResourcesTableName,
                    ProjectionExpression: 'ResourcesType,ResourcesEntity,Version,BuildingName,GroupName,ResourcesID',
                    KeyConditionExpression: 'ResourcesName = :hkey',
                    ExpressionAttributeValues: {
                        ':hkey': `MaeviB:${profileId}:Gateway`
                    }
                };
                var resp = await docClient.query(params).promise();
                var sortItems = lodash.sortBy(resp.Items, ['ResourcesID']);

                var i = 0;
                sortItems.forEach(function(eachItems) {
                    eachItems["key"] = i;
                    i++;
                })

                var response = await successPayload('data', sortItems);
                return response;
            }
            else {
                throw throwError("Parameters not found.")
            }
        }
    }
    catch (error) {
        var response = await errorPayload(error.statusCode, error.message);
        return response;
    }
}
