// ENVIRONMENT
const {
    region,
    infoResourcesTableName,
} = require("./config");

// LIBRARY
const AWS = require("aws-sdk");

// AWS SERVICES
const docClient = new AWS.DynamoDB.DocumentClient({
    apiVersion: "2012-08-10",
    region: region,
});

// GLOBAL FUNCTION
function throwError (message) {
    var payload = {
        statusCode: 400, 
        message: message,
    }
    return payload;
}
async function errorPayload (statusCode, message) {
    try {
        var payload = {
            statusCode: statusCode,
            headers: {
                "Content-Type": "application/json",
                "x-amzn-ErrorType": statusCode
            },
            isBase64Encoded: false,
            body: JSON.stringify({
                message: message,
                statusCode: statusCode,
            })
        }

        return payload
    }
    catch (error) {
        throw error
    }
}
async function successPayload (type, input) {
    try {
        var body = {};
        switch (type) {
            case 'data':
                body = {
                    data: input,
                    statusCode: 200,
                }
                break;
            case 'message':
                body = {
                    message: input,
                    statusCode: 200,
                }
                break;
        }

        var payload = {
            statusCode: 200,
            headers: {
                "Content-Type": "application/json"
            },
            isBase64Encoded: false,
            body: JSON.stringify(body),
        }

        return payload
    }
    catch (error) {
        throw error
    }
}

// MAIN FUNCTION
exports.functionHandler = async (event) => {
    try {
        if (!event.pathParameters || !event.queryStringParameters) {
            throw throwError("Not found.")
        }

        let { profileId } = event.pathParameters;
        let { resourcesEntity } = event.queryStringParameters;

        if (!profileId || !resourcesEntity) {
            throw throwError("Parameters not found.")
        }

        // Check if have gateways.
        var params = {
            TableName : infoResourcesTableName,
            ProjectionExpression: `ResourcesID`,
            KeyConditionExpression: 'ResourcesName = :hkey AND ResourcesEntity = :rkey',
            ExpressionAttributeValues: {
                ':hkey': `MaeviB:${profileId}:Gateway`,
                ':rkey': resourcesEntity
            }
        };

        var queryResp = await docClient.query(params).promise();

        if (queryResp.Items.length == 0) {
            throw throwError("No gateway found.")
        }
        else {
            // delete site
            var params = {
                TableName : infoResourcesTableName,
                Key: {
                    ResourcesName: `MaeviB:${profileId}:Gateway`,
                    ResourcesEntity: resourcesEntity
                }
            };
            await docClient.delete(params).promise()

            // query all gateways per profileid
            var params = {
                TableName : infoResourcesTableName,
                ProjectionExpression: 'ResourcesType,ResourcesEntity,Version,BuildingName,GroupName,ResourcesID',
                KeyConditionExpression: 'ResourcesName = :hkey',
                ExpressionAttributeValues: {
                    ':hkey': `MaeviB:${profileId}:Gateway`
                }
            };
            var resp = await docClient.query(params).promise();
            var sortItems = lodash.sortBy(resp.Items, ['ResourcesID']);

            var i = 0;
            sortItems.forEach(function(eachItems) {
                eachItems["key"] = i;
                i++;
            })

            var response = await successPayload('data', sortItems);
            return response;
        }
    }
    catch (error) {
        var response = await errorPayload(error.statusCode, error.message);
        return response;
    }
}
