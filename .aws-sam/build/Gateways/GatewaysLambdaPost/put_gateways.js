// ENVIRONMENT
const {
    region,
    infoResourcesTableName,
    greengrassResourcesTableName,
} = require("./config");

// LIBRARY
const AWS = require("aws-sdk");
const lodash = require("lodash");
const crypto = require("crypto");
const moment = require("moment");
const q = require("q");

// AWS SERVICES
const docClient = new AWS.DynamoDB.DocumentClient({
    apiVersion: "2012-08-10",
    region: region,
});

// GLOBAL FUNCTION
function throwError (message) {
    var payload = {
        statusCode: 400, 
        message: message,
    }
    return payload;
}
async function errorPayload (statusCode, message) {
    try {
        var payload = {
            statusCode: statusCode,
            headers: {
                "Content-Type": "application/json",
                "x-amzn-ErrorType": statusCode
            },
            isBase64Encoded: false,
            body: JSON.stringify({
                message: message,
                statusCode: statusCode,
            })
        }

        return payload
    }
    catch (error) {
        throw error
    }
}
async function successPayload (type, input) {
    try {
        var body = {};
        switch (type) {
            case 'data':
                body = {
                    data: input,
                    statusCode: 200,
                }
                break;
            case 'message':
                body = {
                    message: input,
                    statusCode: 200,
                }
                break;
        }

        var payload = {
            statusCode: 200,
            headers: {
                "Content-Type": "application/json"
            },
            isBase64Encoded: false,
            body: JSON.stringify(body),
        }

        return payload
    }
    catch (error) {
        throw error
    }
}
function genRandomString() {
    var length = 20;
    return crypto.randomBytes(Math.ceil(length/2))
            .toString('hex') /** convert to hexadecimal format */
            .slice(0,length); /** return required number of characters */
}
function HashSha256(accesskey, salt) {
    var hash = crypto.createHmac('sha256', salt); /** Hashing algorithm sha256 */
    hash.update(accesskey);
    var value = hash.digest('hex');

    return value;
}

// MAIN FUNCTION
exports.functionHandler = async (event) => {
    try {
        if (!event.pathParameters || !event.body) {
            throw throwError("Not found.")
        }

        let { body } = event;
        const incomingBody = JSON.parse(body);

        let { profileId } = event.pathParameters;
        let { gatewayId, siteName, resourcesType } = incomingBody;

        if (!profileId || !gatewayId || !siteName || !resourcesType) {
            throw throwError("Parameters not found.")
        }

        var promises = [];

        // Check if gateway have been register
        var params = {
            TableName : infoResourcesTableName,
            ProjectionExpression: `ResourcesID`,
            KeyConditionExpression: 'ResourcesName = :hkey',
            FilterExpression: 'ResourcesID = :fkey',
            ExpressionAttributeValues: {
                ':hkey': `MaeviB:${profileId}:Gateway`,
                ':fkey': gatewayId
            }
        };
        promises.push(docClient.query(params).promise())

        // Check if building has been registered in account
        var params = {
            TableName : infoResourcesTableName,
            ProjectionExpression: `BuildingName`,
            KeyConditionExpression: 'ResourcesName = :hkey',
            FilterExpression: ' BuildingName = :fkey',
            ExpressionAttributeValues: {
                ':hkey': `MaeviB:${profileId}:Building`,
                ':fkey': siteName
            }
        };
        promises.push(docClient.query(params).promise())

        var queryResp = await q.all(promises);
        var hasGateway = queryResp[0].Items.length > 0 ? true : false;
        var hasBuilding = queryResp[1].Items.length > 0 ? true : false;
        
        if (hasGateway) {
            throw throwError("Gateway have been registered.")
        }
        else if (!hasBuilding) {
            throw throwError("Sites not found.")
        }
        else {
            // Scan gateway in info resources to check isOther 
            var params = {
                TableName : infoResourcesTableName,
                IndexName: 'ResourcesID-Index',
                ProjectionExpression: `ResourcesID`,
                KeyConditionExpression: 'ResourcesID = :hkey',
                ExpressionAttributeValues: {
                    ':hkey': gatewayId,
                }
            };
            var queryResp = await docClient.query(params).promise();  
            var isOther = queryResp.Items.length > 0 ? true : false;

            if (isOther) {
                throw throwError("Gateway belong to other account.")
            }
            else {
                // proceed to register
                switch (resourcesType) {
                    case 'Greengrass_Gateway':
                        // Query gateway in greengrass resources to check isHave
                        var params = {
                            TableName : greengrassResourcesTableName,
                            ProjectionExpression: `GroupName,CoreVersion`,
                            KeyConditionExpression: 'GatewayID = :hkey',
                            ExpressionAttributeValues: {
                                ':hkey': gatewayId
                            }
                        };
                        var queryResp = await docClient.query(params).promise();
                        var isHave = queryResp.Items.length == 1 ? true : false;

                        if (isHave == false) {
                            throw throwError("Gateway not found in the system.")
                        }
                        else {
                            // Proceed to register gateway
                            var groupName = queryResp.Items[0].GroupName;
                            var coreVersion = queryResp.Items[0].CoreVersion;

                            var params = {
                                TableName : infoResourcesTableName,
                                Item: {
                                    ResourcesName: `MaeviB:${profileId}:Gateway`, 
                                    ResourcesEntity: HashSha256(gatewayId, genRandomString()),
                                    BuildingName: siteName,
                                    ResourcesType: resourcesType,
                                    Version: coreVersion,
                                    GroupName: groupName,
                                    ResourcesID: gatewayId,
                                    UpdateTime: moment.utc().startOf('minutes').toISOString(),
                                }
                            };
                            await docClient.put(params).promise();

                            // query all gateways per profileid
                            var params = {
                                TableName : infoResourcesTableName,
                                ProjectionExpression: 'ResourcesType,ResourcesEntity,Version,BuildingName,GroupName,ResourcesID',
                                KeyConditionExpression: 'ResourcesName = :hkey',
                                ExpressionAttributeValues: {
                                    ':hkey': `MaeviB:${profileId}:Gateway`
                                }
                            };
                            var resp = await docClient.query(params).promise();
                            var sortItems = lodash.sortBy(resp.Items, ['ResourcesID']);

                            var i = 0;
                            sortItems.forEach(function(eachItems) {
                                eachItems["key"] = i;
                                i++;
                            })

                            var response = await successPayload('data', sortItems);
                            return response;
                        }
                        break;
                    default:
                        throw throwError("Resource type not found.")
                        break;
                }
            }
        }
    }
    catch (error) {
        var response = await errorPayload(error.statusCode, error.message);
        return response;
    }
}
