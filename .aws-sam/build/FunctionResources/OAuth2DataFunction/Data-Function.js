// NESTED FUNCTION
const BuildingTotalEnergy = require("./nested/BuildingTotalEnergy.js");
const TotalAllEnergy = require("./nested/TotalAllEnergy.js");
const BuildingEnergyIndex = require("./nested/BuildingEnergyIndex.js");
const TotalAllEnergyDayComparison = require("./nested/TotalAllEnergyDayComparison.js");
const TotalResources = require("./nested/TotalResources.js")

// GLOBAL FUNCTION
async function errorPayload (statusCode, message) {
    try {
        var payload = {
            statusCode: statusCode,
            headers: {
                "Content-Type": "application/json",
                "x-amzn-ErrorType": statusCode
            },
            isBase64Encoded: false,
            body: JSON.stringify({
                message: message,
                statusCode: statusCode,
            })
        }

        return payload
    }
    catch (error) {
        throw error
    }
}
async function successPayload (type, input) {
    try {
        var body = {};
        switch (type) {
            case 'data':
                body = {
                    data: input,
                    statusCode: 200,
                }
                break;
            case 'message':
                body = {
                    message: input,
                    statusCode: 200,
                }
                break;
        }

        var payload = {
            statusCode: 200,
            headers: {
                "Content-Type": "application/json"
            },
            isBase64Encoded: false,
            body: JSON.stringify(body),
        }

        return payload
    }
    catch (error) {
        throw error
    }
}

// MAIN FUNCTION
exports.masterHandler = async (event) => {
    try {
        const incomingBody = JSON.parse(event.body);

        switch (incomingBody.command) {
            case 'BuildingTotalEnergy': // get all total energy per building
                var respOut = await BuildingTotalEnergy.nestedHandler(incomingBody);
                var response = await successPayload('data', respOut);
                break;
            case 'TotalAllEnergy':
                var respOut = await TotalAllEnergy.nestedHandler(incomingBody);
                var response = await successPayload('data', respOut);
                break;
            case 'BuildingEnergyIndex':
                var respOut = await BuildingEnergyIndex.nestedHandler(incomingBody);
                var response = await successPayload('data', respOut);
                break;
            case 'TotalAllEnergyDayComparison':
                var respOut = await TotalAllEnergyDayComparison.nestedHandler(incomingBody);
                var response = await successPayload('data', respOut);
                break;
            case 'TotalResources':
                var respOut = await TotalResources.nestedHandler(incomingBody);
                var response = await successPayload('data', respOut);
                break;
            default:
                var response = await errorPayload(400, "Command not found.");
                break;
        }
        console.log(JSON.parse(response.body))
        return response
    }
    catch (error) {
        var response = await errorPayload(error.statusCode, error.message);
        console.log(response)
        return response
    }
}
