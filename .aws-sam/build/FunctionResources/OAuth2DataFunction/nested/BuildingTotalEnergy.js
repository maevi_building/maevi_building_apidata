// ENVIRONMENT
const {
    region,
    infoResourcesTableName,
    dataLoggerTableName
} = require("../config");

// LIBRARY
const AWS = require("aws-sdk");
const q = require("q");

// AWS SERVICES
const docClient = new AWS.DynamoDB.DocumentClient({
    apiVersion: "2012-08-10",
    region: region,
});

// GLOBAL FUNCTION
function deviceCategoryChange(category) {
    var tier = "";
    switch (category) {
        case 'MSB':
            tier = "Tier 1";
            break
        case 'SSB':
            tier = "Tier 2";
            break;
        default:
            tier = category;
            break;
    }

    return tier;
}

// MAIN FUNCTION
exports.nestedHandler = async (event) => {
    try {
        var payload = event.payload;
        var { profileId, date } = payload;

        if (!profileId || !date) {
            throw throwError("Parameter not found.")
        }

        // get list of building
        var params = {
            TableName: infoResourcesTableName,
            ProjectionExpression: 'BuildingName',
            KeyConditionExpression: 'ResourcesName = :hkey',
            ExpressionAttributeValues: {
                ':hkey': `MaeviB:${profileId}:Building`,
            }
        };

        var resp = await docClient.query(params).promise();
        var respOut = [];

        if (resp.Items.length > 0) {
            // get list of devices per building
            var promises = [];
            var devicePerBuilding = [];

            resp.Items.forEach(async (eachBuilding) => {
                var params = {
                    TableName: infoResourcesTableName,
                    IndexName: 'BuildingName-Index',
                    ProjectionExpression: `ResourcesID, ResourcesCategory, ResourcesLabel`,
                    FilterExpression: `contains(ResourcesName, :fkey1) AND ResourcesType = :fkey2`,
                    KeyConditionExpression: 'BuildingName = :hkey',
                    ExpressionAttributeValues: {
                        ':hkey': eachBuilding.BuildingName,
                        ':fkey1': `${profileId}:Device`,
                        ':fkey2': `Power_Meter`,
                    }
                };
                promises.push(docClient.query(params).promise());
            })
            var queryResp = await q.all(promises);

            // reformat the data
            var count = 0;
            resp.Items.forEach(async (eachBuilding) => {
                devicePerBuilding.push({
                    BuildingName: eachBuilding.BuildingName,
                    DeviceList: queryResp[count].Items,
                })
                count++;
            })

            // get EnergyTrend per devices
            var mainPromises = [];
            devicePerBuilding.forEach(async (eachBuilding) => {
                var promises = [];
                eachBuilding.DeviceList.forEach(async (eachDevice) => {
                    var params = {
                        TableName: dataLoggerTableName,
                        ProjectionExpression: `EnergyTrend`,
                        KeyConditionExpression: 'ResourcesID = :hkey AND ResourcesCreated = :skey',
                        ExpressionAttributeValues: {
                            ':hkey': `MaeviB:${eachDevice.ResourcesID}:1MO`,
                            ':skey': date,
                        }
                    };
                    promises.push(docClient.query(params).promise());
                })
                mainPromises.push(q.all(promises))
            })
            var queryResp = await q.all(mainPromises)

            // reformat the data
            var countX = 0;
            var countY = 0;

            devicePerBuilding.forEach(async (eachBuilding) => {
                var perTier = {};
                countY = 0;

                eachBuilding.DeviceList.forEach(async (eachDevice) => {
                    var deviceTier = deviceCategoryChange(eachDevice.ResourcesCategory);
                    var perDevice = {
                        deviceId: eachDevice.ResourcesID,
                        deviceLabel: eachDevice.ResourcesLabel,
                        energy: queryResp[countX][countY].Items.length > 0 ? parseFloat((queryResp[countX][countY].Items[0].EnergyTrend).toFixed(2)) : 0, 
                    }
                    
                    perTier[deviceTier] = perTier[deviceTier] === undefined ? [] : perTier[deviceTier];
                    perTier[deviceTier].push(perDevice);
             
                    countY++;
                })

                // sort data
                var perTierSorted = {};
                Object.keys(perTier).sort().forEach(function(eachKey) {
                    perTierSorted[eachKey] = perTier[eachKey];
                })

                respOut.push({
                    BuildingName: eachBuilding.BuildingName,
                    DeviceList: perTierSorted,
                })
                countX++;
            })
        }
        return respOut
    }
    catch (error) {
        throw error
    }
}
