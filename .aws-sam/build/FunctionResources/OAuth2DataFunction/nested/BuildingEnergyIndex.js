// ENVIRONMENT
const {
    region,
    infoResourcesTableName,
    dataLoggerTableName
} = require("../config");

// LIBRARY
const AWS = require("aws-sdk");
const q = require("q");
const moment = require("moment");
const lodash = require("lodash");

// AWS SERVICES
const docClient = new AWS.DynamoDB.DocumentClient({
    apiVersion: "2012-08-10",
    region: region,
});

// GLOBAL FUNCTION
function throwError (message) {
    var payload = {
        statusCode: 400, 
        message: message,
    }
    return payload;
}


// MAIN FUNCTION
exports.nestedHandler = async (event) => {
    try {
        var payload = event.payload;

        var { profileId, endYear, startYear, currentDate } = payload;

        if (!profileId || !endYear || !startYear || !currentDate) {
            throw throwError("Parameter not found.")
        }

        // get list of building
        var params = {
            TableName: infoResourcesTableName,
            ProjectionExpression: 'BuildingName, GrossFloorArea',
            KeyConditionExpression: 'ResourcesName = :hkey',
            ExpressionAttributeValues: {
                ':hkey': `MaeviB:${profileId}:Building`,
            }
        };

        var resp = await docClient.query(params).promise();
        var respOut = [];

        if (resp.Items.length > 0) {
            // get list of devices per building
            var promises = [];
            var devicePerBuilding = [];

            resp.Items.forEach(async (eachBuilding) => {
                var params = {
                    TableName: infoResourcesTableName,
                    IndexName: 'BuildingName-Index',
                    ProjectionExpression: `ResourcesID`,
                    FilterExpression: `contains(ResourcesName, :fkey1) AND ResourcesType = :fkey2 AND (ResourcesCategory = :fkey4 OR ResourcesCategory = :fkey3)`,
                    KeyConditionExpression: 'BuildingName = :hkey',
                    ExpressionAttributeValues: {
                        ':hkey': eachBuilding.BuildingName,
                        ':fkey1': `${profileId}:Device`,
                        ':fkey2': `Power_Meter`,
                        ':fkey3': 'tier1',
                        ':fkey4': 'MSB',
                    }
                };
                promises.push(docClient.query(params).promise());
            })
            var queryResp = await q.all(promises);

           // reformat the data
            var count = 0;
            resp.Items.forEach(async (eachBuilding) => {
                devicePerBuilding.push({
                    BuildingName: eachBuilding.BuildingName,
                    GrossFloorArea: eachBuilding.GrossFloorArea,
                    DeviceList: queryResp[count].Items,
                })
                count++;
            })

            // get EnergyTrend per devices
            var mainPromises = [];
            devicePerBuilding.forEach(async (eachBuilding) => {
                var promises = [];
                eachBuilding.DeviceList.forEach(async (eachDevice) => {
                    var params = {
                        TableName: dataLoggerTableName,
                        ProjectionExpression: `EnergyTrend, ResourcesCreated`,
                        KeyConditionExpression: 'ResourcesID = :hkey AND ResourcesCreated BETWEEN :todate AND :fromdate',
                        ExpressionAttributeValues: {
                            ':hkey': `MaeviB:${eachDevice.ResourcesID}:1MO`,
                            ':todate': startYear,
                            ':fromdate': endYear,
                        }
                    };
                    promises.push(docClient.query(params).promise());
                })
                mainPromises.push(q.all(promises))
            })
            var queryResp = await q.all(mainPromises)

            // reformat the data
            var countX = 0;
            var countY = 0;

            devicePerBuilding.forEach(async (eachBuilding) => {
                countY = 0;
                var totalEnergyYear = 0;
                eachBuilding.DeviceList.forEach(async (eachDevice) => {
                    queryResp[countX][countY].Items.forEach(async (eachEnergy) => {
                        if (eachEnergy.ResourcesCreated !== startYear) {
                            totalEnergyYear = totalEnergyYear + eachEnergy.EnergyTrend;
                        }
                    })
                    countY++;
                })

                // prorate totalThisMonth
                var diffMillis = moment(currentDate).valueOf() - moment(startYear).valueOf();
                var diffMillisFull = moment(endYear).valueOf() - moment(startYear).valueOf();
                var totalProrateEnergyYear = parseFloat(((totalEnergyYear / diffMillis) * diffMillisFull).toFixed(2));



                var perBuilding = {
                    BuildingName: eachBuilding.BuildingName,
                    GrossFloorArea: parseFloat((parseFloat(eachBuilding.GrossFloorArea)).toFixed(2)),
                    totalEnergyYear: totalEnergyYear,
                    totalProrateEnergyYear: totalProrateEnergyYear,
                    BEI: parseFloat((totalProrateEnergyYear / parseFloat(eachBuilding.GrossFloorArea)).toFixed(2)),
                }

                respOut.push(perBuilding);

                countX++;
            })
        }

        var respOutSorted = lodash.orderBy(respOut, ['BEI'], ['desc']);
    
        return respOutSorted
    }
    catch (error) {
        throw error
    }
}
