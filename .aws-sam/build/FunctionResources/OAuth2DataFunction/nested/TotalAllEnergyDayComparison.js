// ENVIRONMENT
const {
    region,
    infoResourcesTableName,
    dataLoggerTableName
} = require("../config");

// LIBRARY
const AWS = require("aws-sdk");
const q = require("q");
const moment = require("moment");
const lodash = require("lodash");
const si = require('si-prefix');

// AWS SERVICES
const docClient = new AWS.DynamoDB.DocumentClient({
    apiVersion: "2012-08-10",
    region: region,
});

// GLOBAL FUNCTION


// MAIN FUNCTION
exports.nestedHandler = async (event) => {
    try {
        var payload = event.payload;

        var { profileId, currentDate, gmt } = payload;

        if (!profileId || !currentDate || !gmt) {
            throw throwError("Parameter not found.")
        }

        // get list of building
        var params = {
            TableName: infoResourcesTableName,
            ProjectionExpression: 'BuildingName',
            KeyConditionExpression: 'ResourcesName = :hkey',
            ExpressionAttributeValues: {
                ':hkey': `MaeviB:${profileId}:Building`,
            }
        };

        var resp = await docClient.query(params).promise();
        var respOut = [];

        if (resp.Items.length > 0) {
            // get list of devices per building
            var promises = [];

            resp.Items.forEach(async (eachBuilding) => {
                var params = {
                    TableName: infoResourcesTableName,
                    IndexName: 'BuildingName-Index',
                    ProjectionExpression: `ResourcesID, ResourcesCategory`,
                    FilterExpression: `contains(ResourcesName, :fkey1) AND ResourcesType = :fkey2 AND (ResourcesCategory = :fkey4 OR ResourcesCategory = :fkey3)`,
                    KeyConditionExpression: 'BuildingName = :hkey',
                    ExpressionAttributeValues: {
                        ':hkey': eachBuilding.BuildingName,
                        ':fkey1': `${profileId}:Device`,
                        ':fkey2': `Power_Meter`,
                        ':fkey3': 'tier1',
                        ':fkey4': 'MSB',
                    }
                };
                promises.push(docClient.query(params).promise());
            })
            var queryResp = await q.all(promises);

            // reformat the data
            var deviceList = [];
            queryResp.forEach(async (eachResp) => {
                eachResp.Items.forEach(function(eachDevice) {
                    deviceList.push(eachDevice)
                })
            })

            // get date from and to
            var gmtHour = parseInt(`${gmt.split('+')[1][0] + gmt.split('+')[1][1]}`);
            var subtractHour = (moment.utc(currentDate).hours() + gmtHour) > 24 ? ((moment.utc(currentDate).hours() + gmtHour) - 24) : (moment.utc(currentDate).hours() + gmtHour);

            var currentDateFrom = currentDate;
            var currentDateTo = moment(currentDate).subtract(subtractHour - 1,'hour').toISOString();
            var sevenDaybackFrom = moment(currentDateFrom).subtract(7,'day').add(24 - subtractHour,'hour').toISOString();
            var sevenDaybackTo = moment(sevenDaybackFrom).subtract(23,'hour').toISOString();
            // var sevenDaybackFrom = moment(currentDateFrom).subtract(7,'day').toISOString();
            // var sevenDaybackTo = moment(sevenDaybackFrom).subtract(subtractHour - 1,'hour').toISOString();

            var currentTimeFrame = [];
            var pastTimeFrame = [];

            for (var i = 0; i < 24; i++) {
                currentTimeFrame.push(moment(currentDateTo).add(i,'hour').toISOString());
                pastTimeFrame.push(moment(sevenDaybackTo).add(i,'hour').toISOString());
            }

            /*for (var i = 0; i < subtractHour; i++) {
                currentTimeFrame.push(moment(currentDateTo).add(i,'hour').toISOString());
                pastTimeFrame.push(moment(sevenDaybackTo).add(i,'hour').toISOString());
            }*/

            var allTimeFrame = [ currentTimeFrame, pastTimeFrame ];

            // get EnergyTrend per devices
            var promises1 = [];
            var promises2 = [];
            var mainPromises = [];

            deviceList.forEach(async (eachDevice) => {
                var params1 = {
                    TableName: dataLoggerTableName,
                    ProjectionExpression: `EnergyTrend, ResourcesCreated`,
                    KeyConditionExpression: 'ResourcesID = :hkey AND ResourcesCreated BETWEEN :todate AND :fromdate',
                    ScanIndexForward: true,
                    ExpressionAttributeValues: {
                        ':hkey': `MaeviB:${eachDevice.ResourcesID}:1H`,
                        ':todate': currentDateTo,
                        ':fromdate': currentDateFrom,
                    }
                };
                var params2 = {
                    TableName: dataLoggerTableName,
                    ProjectionExpression: `EnergyTrend, ResourcesCreated`,
                    KeyConditionExpression: 'ResourcesID = :hkey AND ResourcesCreated BETWEEN :todate AND :fromdate',
                    ScanIndexForward: true,
                    ExpressionAttributeValues: {
                        ':hkey': `MaeviB:${eachDevice.ResourcesID}:1H`,
                        ':todate': sevenDaybackTo,
                        ':fromdate': sevenDaybackFrom,
                    }
                };

                promises1.push(docClient.query(params1).promise());
                promises2.push(docClient.query(params2).promise());
            })
            mainPromises.push(q.all(promises1))
            mainPromises.push(q.all(promises2))

            var queryResp = await q.all(mainPromises);
            
            // reformat data and total 
            var totalData = [];
            var countPromises = 0;
            queryResp.forEach(function(eachPromises) {
                var sum24 = [];
                eachPromises.forEach(function(eachDevice) {
                    var countItems = 0;
                    allTimeFrame[countPromises].forEach(function(eachTime) {

                        var found = lodash.find(eachDevice.Items, function(o) { return o.ResourcesCreated == eachTime; });

                        if (found) {
                            var sum = (sum24[countItems] === undefined ? found : { ResourcesCreated: found.ResourcesCreated, EnergyTrend: (sum24[countItems].EnergyTrend + found.EnergyTrend) });
                            sum24[countItems] = sum;
                        }
                        else {
                            sum24[countItems] = sum24[countItems] === undefined ? { ResourcesCreated: eachTime, EnergyTrend: 0 } : sum24[countItems];
                        }

                        countItems++;                   
                    })
                })
                totalData.push(sum24);
                countPromises++;
            })

            // filter data
            var filteredTotalData = [];
            totalData.forEach(function(eachDataSet) {
                var newDataSet = [];
                eachDataSet.forEach(function(eachData) {
                    eachData.EnergyTrend = parseFloat((eachData.EnergyTrend).toFixed(2));
                    if (eachData.EnergyTrend === 0) {
                        eachData = lodash.pick(eachData, ['ResourcesCreated']);
                    }
                    newDataSet.push(eachData)
                })
                filteredTotalData.push(newDataSet)
            })

            // polarate latest value
            if (filteredTotalData[0].length > 0){
                filteredTotalData[0][subtractHour - 1].EnergyTrend = (filteredTotalData[0][subtractHour - 1].EnergyTrend / (moment.utc().minute() * 60 * 1000)) * (60 * 60 * 1000);
                filteredTotalData[0][subtractHour - 1].EnergyTrend = parseFloat(filteredTotalData[0][subtractHour - 1].EnergyTrend.toFixed(2));
            }

            respOut = filteredTotalData;
        }
        
        return respOut
    }
    catch (error) {
        throw error
    }
}
