module.exports = {
	// dynamoDB
	infoResourcesTableName: process.env.INFO_RESOURCES_TABLE_NAME || 'develop-maevibbackend-Resources-1V7L4S5JOWYCV-DynamoDBMaeviBInfoResources-WQFYXU6DMI5U',
    dataLoggerTableName: process.env.LOGGER_TABLE_NAME,
    ttlDataDownloadTableName: process.env.TTL_DATA_DOWNLOAD_TABLE_NAME,
    
    // s3
    saveDownloadBucket: process.env.DOWNLOAD_BIG_DATA_BUCKET_NAME,

	// cloud Info
	region: process.env.REGION || 'ap-southeast-1',    
}