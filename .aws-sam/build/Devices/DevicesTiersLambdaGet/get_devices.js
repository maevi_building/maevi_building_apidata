// ENVIRONMENT
const {
    region,
    infoResourcesTableName,
    deviceSubscriptionTableName,
} = require("./config");

// LIBRARY
const AWS = require("aws-sdk");
const lodash = require("lodash");

// AWS SERVICES
const docClient = new AWS.DynamoDB.DocumentClient({
    apiVersion: "2012-08-10",
    region: region,
});

// GLOBAL FUNCTION
function throwError (message) {
    var payload = {
        statusCode: 400, 
        message: message,
    }
    return payload;
}
async function errorPayload (statusCode, message) {
    try {
        var payload = {
            statusCode: statusCode,
            headers: {
                "Content-Type": "application/json",
                "x-amzn-ErrorType": statusCode
            },
            isBase64Encoded: false,
            body: JSON.stringify({
                message: message,
                statusCode: statusCode,
            })
        }

        return payload
    }
    catch (error) {
        throw error
    }
}
async function successPayload (type, input) {
    try {
        var body = {};
        switch (type) {
            case 'data':
                body = {
                    data: input,
                    statusCode: 200,
                }
                break;
            case 'message':
                body = {
                    message: input,
                    statusCode: 200,
                }
                break;
        }

        var payload = {
            statusCode: 200,
            headers: {
                "Content-Type": "application/json"
            },
            isBase64Encoded: false,
            body: JSON.stringify(body),
        }

        return payload
    }
    catch (error) {
        throw error
    }
}

// MAIN FUNCTION
exports.functionHandler = async (event) => {
    try {
    	if (!event.pathParameters) {
            throw throwError("Path not found.")
    	}

    	let { profileId, resourcesType  } = event.pathParameters;

    	if (!profileId || !resourcesType) {
            throw throwError("Parameters not found.")
    	}

        if (resourcesType == "powermeter") {
            resourcesType = "Power_Meter"
        } else if (resourcesType == "thdsensor") {
            resourcesType = "THD_Sensor"
        } else {
            throw throwError("Parameters not found.")
        }
        
    	var params = {
			TableName : infoResourcesTableName,
            ProjectionExpression: 'ResourcesType,ResourcesCategory,SerialNumber,GatewayID,ResourcesEntity,ModelNumber,ResourcesLocation,BuildingName,ResourcesLabel,ResourcesID',
			KeyConditionExpression: 'ResourcesName = :hkey',
            FilterExpression: 'ResourcesType = :fkey',
			ExpressionAttributeValues: {
				':hkey': `MaeviB:${profileId}:Device`,
                ':fkey': resourcesType
			}
		};
		var resp = await docClient.query(params).promise();
        var sortItems = lodash.sortBy(resp.Items, ['BuildingName']);

        var i = 0;
        sortItems.forEach(function(eachItems) {
            eachItems["key"] = i;
            i++;
        })

		var response = await successPayload('data', sortItems);
		return response;
    }
    catch (error) {
        var response = await errorPayload(error.statusCode, error.message);
        return response;
    }
}
