// ENVIRONMENT
const {
    region,
    infoResourcesTableName,
    deviceSubscriptionTableName,
} = require("./config");

// LIBRARY
const AWS = require("aws-sdk");
const lodash = require("lodash");
const crypto = require("crypto");
const moment = require("moment");
const q = require("q");

// AWS SERVICES
const docClient = new AWS.DynamoDB.DocumentClient({
    apiVersion: "2012-08-10",
    region: region,
});

// GLOBAL FUNCTION
function throwError (message) {
    var payload = {
        statusCode: 400, 
        message: message,
    }
    return payload;
}
async function errorPayload (statusCode, message) {
    try {
        var payload = {
            statusCode: statusCode,
            headers: {
                "Content-Type": "application/json",
                "x-amzn-ErrorType": statusCode
            },
            isBase64Encoded: false,
            body: JSON.stringify({
                message: message,
                statusCode: statusCode,
            })
        }

        return payload
    }
    catch (error) {
        throw error
    }
}
async function successPayload (type, input) {
    try {
        var body = {};
        switch (type) {
            case 'data':
                body = {
                    data: input,
                    statusCode: 200,
                }
                break;
            case 'message':
                body = {
                    message: input,
                    statusCode: 200,
                }
                break;
        }

        var payload = {
            statusCode: 200,
            headers: {
                "Content-Type": "application/json"
            },
            isBase64Encoded: false,
            body: JSON.stringify(body),
        }

        return payload
    }
    catch (error) {
        throw error
    }
}
function genRandomString() {
    var length = 20;
    return crypto.randomBytes(Math.ceil(length/2))
            .toString('hex') /** convert to hexadecimal format */
            .slice(0,length); /** return required number of characters */
}
function HashSha256(accesskey, salt) {
    var hash = crypto.createHmac('sha256', salt); /** Hashing algorithm sha256 */
    hash.update(accesskey);
    var value = hash.digest('hex');

    return value;
}

// MAIN FUNCTION
exports.functionHandler = async (event) => {
    try {
        if (!event.pathParameters || !event.body) {
            throw throwError("Not found.")
        }

        let { body } = event;
        const incomingBody = JSON.parse(body);

        let { profileId, resourcesType } = event.pathParameters;
        let { modelNumber, tier, label, serialNumber, location, gatewayId, siteName } = incomingBody;

        if (resourcesType == "powermeter" && (!profileId || !modelNumber || !tier || !label || !serialNumber || !location || !gatewayId || !siteName)) {
            throw throwError("Parameters not found.")
        } else if (resourcesType == "thdsensor" && (!profileId || !modelNumber || !label || !serialNumber || !location || !gatewayId || !siteName)) {
            throw throwError("Parameters not found.")
        }

        if (resourcesType == "powermeter") {
            resourcesType = "Power_Meter"
        } else if (resourcesType == "thdsensor") {
            resourcesType = "THD_Sensor"
        } else {
            throw throwError("Parameters not found.")
        }

        var promises = []

        // Check if incoming serial and model have registered in DB
        var params = {
            TableName : infoResourcesTableName,
            IndexName: 'ResourcesID-Index',
            ProjectionExpression: `ResourcesID`,
            KeyConditionExpression: 'ResourcesID = :hkey',
            ExpressionAttributeValues: {
                ':hkey': `${modelNumber}-${serialNumber}`
            }
        }; 
        promises.push(docClient.query(params).promise());

        // Check if gateway has been register
        var params = {
            TableName : infoResourcesTableName,
            IndexName: 'ResourcesID-Index',
            ProjectionExpression: `ResourcesID`,
            KeyConditionExpression: 'ResourcesID = :hkey',
            FilterExpression: 'ResourcesName = :fkey',
            ExpressionAttributeValues: {
                ':hkey': gatewayId,
                ':fkey': `MaeviB:${profileId}:Gateway`
            }
        }; 
        promises.push(docClient.query(params).promise());

        // Check if sites has been register
        var params = {
            TableName : infoResourcesTableName,
            IndexName: 'BuildingName-Index',
            ProjectionExpression: `BuildingName`,
            KeyConditionExpression: 'BuildingName = :hkey',
            FilterExpression: 'ResourcesName = :fkey',
            ExpressionAttributeValues: {
                ':hkey': siteName,
                ':fkey': `MaeviB:${profileId}:Building`
            }
        }; 
        promises.push(docClient.query(params).promise());
        
        var queryResp = await q.all(promises);

        var haveDevice = queryResp[0].Items.length > 0 ? true : false;
        var haveGateway = queryResp[1].Items.length > 0 ? true : false;
        var haveSite = queryResp[2].Items.length > 0 ? true : false;

        if (haveDevice) {
            throw throwError("Device has been registered in the system.")
        } else if (!haveGateway || !haveSite) {
            throw throwError("Register requirement not complete.")
        } else {
            switch (resourcesType) {
                case "Power_Meter": 
                    // proceed to register power meter
                    var params = {
                        TableName : infoResourcesTableName,
                        Item: {
                            "BuildingName": siteName,
                            "GatewayID": gatewayId,
                            "ModelNumber": modelNumber,
                            "ResourcesCategory": `Tier ${tier}`,
                            "ResourcesEntity": HashSha256(`${modelNumber}-${serialNumber}`, genRandomString()),
                            "ResourcesID": `${modelNumber}-${serialNumber}`,
                            "ResourcesLabel": label,
                            "ResourcesLocation": location,
                            "ResourcesName": `MaeviB:${profileId}:Device`,
                            "ResourcesType": resourcesType,
                            "SerialNumber": serialNumber,
                            "UpdateTime": moment.utc().startOf('minutes').toISOString()
                        }
                    };
                    await docClient.put(params).promise();
                    break;
                case "THD_Sensor":
                    // proceed to register thd sensor
                    var params = {
                        TableName : infoResourcesTableName,
                        Item: {
                            "BuildingName": siteName,
                            "GatewayID": gatewayId,
                            "ModelNumber": modelNumber,
                            "ResourcesEntity": HashSha256(`${modelNumber}-${serialNumber}`, genRandomString()),
                            "ResourcesID": `${modelNumber}-${serialNumber}`,
                            "ResourcesLabel": label,
                            "ResourcesLocation": location,
                            "ResourcesName": `MaeviB:${profileId}:Device`,
                            "ResourcesType": resourcesType,
                            "SerialNumber": serialNumber,
                            "UpdateTime": moment.utc().startOf('minutes').toISOString()
                        }
                    };
                    await docClient.put(params).promise();
                    break;
                default: 
                    throw throwError("Resource type not found.")
                    break;
            }
        }

        // query all power meter per profileid
        var params = {
			TableName : infoResourcesTableName,
            ProjectionExpression: 'ResourcesType,ResourcesCategory,SerialNumber,GatewayID,ResourcesEntity,ModelNumber,ResourcesLocation,BuildingName,ResourcesLabel,ResourcesID',
			KeyConditionExpression: 'ResourcesName = :hkey',
            FilterExpression: 'ResourcesType = :fkey',
			ExpressionAttributeValues: {
				':hkey': `MaeviB:${profileId}:Device`,
                ':fkey': resourcesType
			}
		};
		var resp = await docClient.query(params).promise();
        var sortItems = lodash.sortBy(resp.Items, ['BuildingName']);

        var i = 0;
        sortItems.forEach(function(eachItems) {
            eachItems["key"] = i;
            i++;
        })

		var response = await successPayload('data', sortItems);
		return response;
    }
    catch (error) {
        var response = await errorPayload(error.statusCode, error.message);
        return response;
    }
}
