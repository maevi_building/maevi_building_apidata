// ENVIRONMENT
const {
    region,
    infoResourcesTableName,
} = require("./config");

// LIBRARY
const AWS = require("aws-sdk");
const q = require("q");

// AWS SERVICES
const docClient = new AWS.DynamoDB.DocumentClient({
    apiVersion: "2012-08-10",
    region: region,
});

// GLOBAL FUNCTION
function throwError (message) {
    var payload = {
        statusCode: 400, 
        message: message,
    }
    return payload;
}
async function errorPayload (statusCode, message) {
    try {
        var payload = {
            statusCode: statusCode,
            headers: {
                "Content-Type": "application/json",
                "x-amzn-ErrorType": statusCode
            },
            isBase64Encoded: false,
            body: JSON.stringify({
                message: message,
                statusCode: statusCode,
            })
        }

        return payload
    }
    catch (error) {
        throw error
    }
}
async function successPayload (type, input) {
    try {
        var body = {};
        switch (type) {
            case 'data':
                body = {
                    data: input,
                    statusCode: 200,
                }
                break;
            case 'message':
                body = {
                    message: input,
                    statusCode: 200,
                }
                break;
        }

        var payload = {
            statusCode: 200,
            headers: {
                "Content-Type": "application/json"
            },
            isBase64Encoded: false,
            body: JSON.stringify(body),
        }

        return payload
    }
    catch (error) {
        throw error
    }
}

// MAIN FUNCTION
exports.functionHandler = async (event) => {
    try {
    	if (!event.pathParameters || !event.queryStringParameters) {
            throw throwError("Path not found.")
    	}

    	let { profileId, resourcesType, tier  } = event.pathParameters;
        let { siteName } = event.queryStringParameters;

    	if (!profileId || !resourcesType || !tier || !siteName || !hierarchyType) {
            throw throwError("Parameters not found.")
    	}

        if (resourcesType == "powermeter") {
            resourcesType = "Power_Meter"
        } else {
            throw throwError("Parameters not found.")
        }

        if (hierarchyType == 'parent') {
            // Query Device per Site per tier
            var params = {
                TableName : infoResourcesTableName,
                IndexName: 'BuildingName-Index',
                ProjectionExpression: `ResourcesID, ResourcesLabel`,
                KeyConditionExpression: 'BuildingName = :hkey',
                FilterExpression: 'ResourcesName = :fkey1 AND ResourcesCategory = :fkey2',
                ExpressionAttributeValues: {
                    ':hkey': siteName,
                    ':fkey1': `MaeviB:${profileId}:Device`,
                    ':fkey2': tier
                }
            }; 
            var queryResp = await docClient.query(params).promise();

            var i = 0;
            queryResp.Items.forEach(function(eachItems) {
                eachItems["key"] = i;
                i++;
            })

            var response = await successPayload('data', queryResp.Items);
            return response;
        } else if (hierarchyType == 'child') {
            var promises = [];
            // Query Device per Site per tier
            var params = {
                TableName : infoResourcesTableName,
                IndexName: 'BuildingName-Index',
                ProjectionExpression: `ResourcesID, ResourcesLabel`,
                KeyConditionExpression: 'BuildingName = :hkey',
                FilterExpression: 'ResourcesName = :fkey1 AND ResourcesCategory = :fkey2',
                ExpressionAttributeValues: {
                    ':hkey': siteName,
                    ':fkey1': `MaeviB:${profileId}:Device`,
                    ':fkey2': tier
                }
            }; 
            promises.push(docClient.query(params).promise());
            

            var promisesResp = await q.all(promises);

            // var i = 0;
            // queryResp.Items.forEach(function(eachItems) {
            //     eachItems["key"] = i;
            //     i++;
            // })

            // var response = await successPayload('data', queryResp.Items);
            // return response;

        } else {
            throw throwError("Parameters not found.")
        }
    }
    catch (error) {
        var response = await errorPayload(error.statusCode, error.message);
        return response;
    }
}
