// ENVIRONMENT
const {
    region,
    infoResourcesTableName,
} = require("./config");

// LIBRARY
const AWS = require("aws-sdk");
const lodash = require("lodash");
const moment = require("moment");
const q = require("q");

// AWS SERVICES
const docClient = new AWS.DynamoDB.DocumentClient({
    apiVersion: "2012-08-10",
    region: region,
});

// GLOBAL FUNCTION
function throwError (message) {
    var payload = {
        statusCode: 400, 
        message: message,
    }
    return payload;
}
async function errorPayload (statusCode, message) {
    try {
        var payload = {
            statusCode: statusCode,
            headers: {
                "Content-Type": "application/json",
                "x-amzn-ErrorType": statusCode
            },
            isBase64Encoded: false,
            body: JSON.stringify({
                message: message,
                statusCode: statusCode,
            })
        }

        return payload
    }
    catch (error) {
        throw error
    }
}
async function successPayload (type, input) {
    try {
        var body = {};
        switch (type) {
            case 'data':
                body = {
                    data: input,
                    statusCode: 200,
                }
                break;
            case 'message':
                body = {
                    message: input,
                    statusCode: 200,
                }
                break;
        }

        var payload = {
            statusCode: 200,
            headers: {
                "Content-Type": "application/json"
            },
            isBase64Encoded: false,
            body: JSON.stringify(body),
        }

        return payload
    }
    catch (error) {
        throw error
    }
}

// MAIN FUNCTION
exports.functionHandler = async (event) => {
    try {
        if (!event.pathParameters || !event.body) {
            throw throwError("Not found.")
        }

        let { body } = event;
        const incomingBody = JSON.parse(body);

        let { profileId, resourcesType } = event.pathParameters;
        let { ResourcesEntity, GatewayID, BuildingName } = incomingBody;

        if (!profileId || !ResourcesEntity || !GatewayID || !BuildingName) {
            throw throwError("Parameters not found.")
        }
        
        var promises = []

        // Check if have device.
        var params = {
            TableName : infoResourcesTableName,
            ProjectionExpression: `ResourcesID`,
            KeyConditionExpression: 'ResourcesName = :hkey AND ResourcesEntity = :rkey',
            ExpressionAttributeValues: {
                ':hkey': `MaeviB:${profileId}:Device`,
                ':rkey': ResourcesEntity
            }
        };
        promises.push(docClient.query(params).promise());

        // Check if gateway has been register
        var params = {
            TableName : infoResourcesTableName,
            IndexName: 'ResourcesID-Index',
            ProjectionExpression: `ResourcesID`,
            KeyConditionExpression: 'ResourcesID = :hkey',
            FilterExpression: 'ResourcesName = :fkey',
            ExpressionAttributeValues: {
                ':hkey': GatewayID,
                ':fkey': `MaeviB:${profileId}:Gateway`
            }
        }; 
        promises.push(docClient.query(params).promise());

        // Check if sites has been register
        var params = {
            TableName : infoResourcesTableName,
            IndexName: 'BuildingName-Index',
            ProjectionExpression: `BuildingName`,
            KeyConditionExpression: 'BuildingName = :hkey',
            FilterExpression: 'ResourcesName = :fkey',
            ExpressionAttributeValues: {
                ':hkey': BuildingName,
                ':fkey': `MaeviB:${profileId}:Building`
            }
        }; 
        promises.push(docClient.query(params).promise());
        var queryResp = await q.all(promises);

        var noDevice = queryResp[0].Items.length == 0 ? true : false;
        var noGateway = queryResp[1].Items.length == 0 ? true : false;
        var noSite = queryResp[2].Items.length == 0 ? true : false;

        if (noDevice) {
            throw throwError("Device not found.")
        } else if (noGateway || noSite) {
            throw throwError("Register requirement not complete.")
        } else {
            // proceed to update
            var updateExpression = [];
            var expressionAttributeNames = {};
            var needUpdate = false;

            expressionAttributeValues = {};
            
            for (key in incomingBody) {
                if (key == "ResourcesCategory") {
                    incomingBody[key] = `Tier ${incomingBody[key]}`;
                }
                if (key != "ResourcesEntity") {
                    needUpdate = true;
                    updateExpression.push(`#${key} = :${key}`);
                    expressionAttributeNames[`#${key}`] = key;
                    expressionAttributeValues[`:${key}`] = incomingBody[key];
                }
            }

            // update time
            updateExpression.push(`#UpdateTime = :UpdateTime`);
            expressionAttributeNames[`#UpdateTime`] = "UpdateTime";
            expressionAttributeValues[`:UpdateTime`] = moment.utc().startOf('minutes').toISOString();

            updateExpression = 'set ' + updateExpression.join(",");

            if (needUpdate) {
                var params = {
                    TableName : infoResourcesTableName,
                    Key: {
                        ResourcesName: `MaeviB:${profileId}:Device`,
                        ResourcesEntity: ResourcesEntity
                    },
                    UpdateExpression: updateExpression,
                    ExpressionAttributeNames: expressionAttributeNames,
                    ExpressionAttributeValues: expressionAttributeValues
                }
                await docClient.update(params).promise();

                // query all devices per profileid
                if (resourcesType == "powermeter") {
                    resourcesType = "Power_Meter"
                } else if (resourcesType == "thdsensor") {
                    resourcesType = "THD_Sensor"
                } else {
                    throw throwError("Parameters not found.")
                }
                
                var params = {
                    TableName : infoResourcesTableName,
                    ProjectionExpression: 'ResourcesType,ResourcesCategory,SerialNumber,GatewayID,ResourcesEntity,ModelNumber,ResourcesLocation,BuildingName,ResourcesLabel,ResourcesID',
                    KeyConditionExpression: 'ResourcesName = :hkey',
                    FilterExpression: 'ResourcesType = :fkey',
                    ExpressionAttributeValues: {
                        ':hkey': `MaeviB:${profileId}:Device`,
                        ':fkey': resourcesType
                    }
                };
                var resp = await docClient.query(params).promise();
                var sortItems = lodash.sortBy(resp.Items, ['BuildingName']);
        
                var i = 0;
                sortItems.forEach(function(eachItems) {
                    eachItems["key"] = i;
                    i++;
                })
        
                var response = await successPayload('data', sortItems);
                return response;
            }
            else {
                throw throwError("Parameters not found.")
            }
        }
    }
    catch (error) {
        var response = await errorPayload(error.statusCode, error.message);
        return response;
    }
}
