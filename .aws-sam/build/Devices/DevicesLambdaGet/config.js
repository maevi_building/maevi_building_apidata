module.exports = {
	// dynamoDB
	infoResourcesTableName: process.env.INFO_RESOURCES_TABLE_NAME || 'develop-maevibbackend-Resources-1V7L4S5JOWYCV-DynamoDBMaeviBInfoResources-WQFYXU6DMI5U',
	deviceSubscriptionTableName: process.env.DEVICE_SUBSCRIPTION_TABLE_NAME || 'dev-DB-MaeviBuilding-Subscription-DeviceSubscription',
	// cloud Info
	region: process.env.REGION || 'ap-southeast-1',
}