var handler = require("../src/handlers/Data-Function.js");
var moment = require("moment");

var profileId = "MaeviBDemo";

test('Test 1: success test', async () => { 
    var params = {
        body: JSON.stringify({
            command: "TotalAllEnergyDayComparison",
            payload: {
                profileId: profileId,
                currentDate: moment().endOf('hour').add(1,'millisecond').toISOString(),
                gmt: moment().toString().split(' ')[5]
            }
        }),
    }

    var response = await handler.masterHandler(params);

    expect(response).toMatchObject({ 
        statusCode: 200,
        headers: { "Content-Type": "application/json" },
        isBase64Encoded: false,
    });
}, 15000); 

