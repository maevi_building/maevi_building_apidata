var handler = require("../src/handlers/Data-Function.js");
var moment = require("moment");

var profileId = "MaeviBDemo";

test('Test 1: success test', async () => { 
    var params = { 
        headers: {
            authorization: 'Basic c3lhbWltX25zbEB5YWhvby5jb206MTIzNDU2Nzg5',
        },
        body: JSON.stringify({
            command: "BuildingEnergyIndex",
            payload: {
                profileId: profileId,
                currentDate: moment().toISOString(),
                endYear: moment().endOf('year').add(1,'millisecond').toISOString(),
                startYear: moment().startOf('year').toISOString(),
            }
        }),
    }

    var response = await handler.masterHandler(params);

    expect(response).toMatchObject({ 
        statusCode: 200,
        headers: { "Content-Type": "application/json" },
        isBase64Encoded: false,
    });
}, 15000); 

