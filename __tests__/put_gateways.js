var handler = require("../src/gateways/put_gateways.js");

var profileId = "MaeviBDemo";

test('Test 1: success test', async () => { 
    var params = { 
        "pathParameters": {
            profileId: profileId
        },
        "body": JSON.stringify({
            gatewayId: "KSA4037210",
            buildingName: "Maevi Dev",
            resourcesType: "Greengrass_Gateway",
        })
    }
    var response = await handler.functionHandler(params);

    expect(response).toMatchObject({ 
        statusCode: 200,
        headers: { "Content-Type": "application/json" },
        isBase64Encoded: false,
    });
}, 15000); 

