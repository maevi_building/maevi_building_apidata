var handler = require("../src/sites/delete_sites.js");

var profileId = "MaeviBDemo";

test('Test 1: success test', async () => { 
    var params = { 
        "pathParameters": {
            profileId: profileId
        },
        "queryStringParameters": {
            "resourcesEntity": "nhuhuhu"
        }
    }
    var response = await handler.functionHandler(params);

    expect(response).toMatchObject({ 
        statusCode: 200,
        headers: { "Content-Type": "application/json" },
        isBase64Encoded: false,
    });
}, 15000); 

